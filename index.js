const express = require('express')
const redis = require('redis')

const app = express();

const client = redis.createClient({
    host: 'nodejs-helloworld-dev' || 'nodejs-helloworld-prod', //Redis Host IP Address will be clusterIP of NodePort service type in kubernetes
});

console.log(host);

client.set('string','Hello World wassup!!! Checking if string value changes'); 

app.get('/', (req,res) => {
    client.get('string', (err,string) => {
        res.send(string); 
    });
});

app.listen(8081, () => {
    console.log('Listening on port 8081');
});